// server.js
// set up ======================================================================
var express  = require('express'),
	app      = express(),
	port     = process.env.PORT || 80
	mongoose = require('mongoose'),
	passport = require('passport')
	flash    = require('connect-flash')
	request  = require('request');

var morgan      	= require('morgan'),
	cookieParser 	= require('cookie-parser'),
	bodyParser   	= require('body-parser'),
	session      	= require('express-session');

var configDB = require('./config/database.js');

// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({ secret: 'XDMWISNJCJFJKFJSMJDKIO' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
app.use(express.static(__dirname + '/assets'));//use an static folder for assets
//FORCE SSL
/*
app.use(function(req, res, next) {
  if(req.headers['x-forwarded-proto']==='http') {
    return res.redirect(['https://', req.get('Host'), req.url].join(''));
  }
  next();
});
*/
// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
var server = app.listen(port,function(){
	console.log("CloudFile server started at  "+port);
});
