WL.init({ client_id: '0000000048121901', redirect_uri: 'http://www.cloudfileapp.com/connect/skydrive' });

            WL.login({ "scope": "wl.basic wl.skydrive" }).then(
                function (response) {
                    
                    getFiles();

                },
                function (response) {
                    console.log("Could not connect, status = " + response.status);
                }
            );
 

function getFiles() {
    var files_path = "/me/skydrive/files";
    WL.api({ path: files_path, method: "GET" }).then(
        onGetFilesComplete,
        function(response) {
            log("Cannot get files and folders: " +
                JSON.stringify(response.error).replace(/,/g, ",\n"));
        }
    );
}

function onGetFilesComplete(response) {
    var items = response.data;
    var foundFolder = 0;
    for (var i = 0; i < items.length; i++) {
        if (items[i].type === "folder") {
            log("Found a folder with the following information: " +
                JSON.stringify(items[i]).replace(/,/g, ",\n"));
            foundFolder = 1;
            break;
        }
    }

    if (foundFolder == 0) {
        log("Unable to find any folders");
    }
}
                     
function log(message) {
    var child = document.createTextNode(message);
    var parent = document.getElementById('JsOutputDiv') || document.body;
    parent.appendChild(child);
    parent.appendChild(document.createElement("br"));
}