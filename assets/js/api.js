angular.module('api', []).controller('api',function($scope, $http){
	$http.get('/api/files/dropbox/root').success(function(data){
		$scope.dropbox = data.contents;		
	});	
	$http.get('/api/files/google/root').success(function(data){
		if(data.items){
			$scope.google = data.items;
		}else{
			window.location.replace("/refreshtoken/google");
		}
	});
	$http.get('/api/files/box/root').success(function(data){
		if(data){
			$scope.box = data.item_collection.entries;
		}else{
			window.location.replace("/refreshtoken/box");
		}	
	});
	/*	
	$http.get('/connect/skydrive').success(function(data){
		$scope.skydrive = data;
		console.log(data);
	});
	*/
});


/*
 $http.get('/api/files/google/root').success(function(data){
		$scope.google = data;
	});
	$http.get('/api/files/box/root').success(function(data){
		$scope.box = data;
	});
	$http.get('/connect/skydrive').success(function(data){
		$scope.skydrive = data;
	});
 * 
 */
