var express 	= require('express');
var app 		= express();
var request 	= require('request');
var configAuth 	= require('../config/auth');

var g_client_id 	= configAuth.googleAuth.clientID,
	g_client_secret = configAuth.googleAuth.clientSecret,
	g_scope 		= "https://www.googleapis.com/auth/drive",
	g_redirect_uri 	= configAuth.googleAuth.callbackURL;

var g_auth_url ="https://accounts.google.com/o/oauth2/auth"
				+"?client_id="		+g_client_id
				+"&redirect_uri="	+g_redirect_uri
				+"&scope="			+g_scope
				+"&response_type=code"
				+"&access_type=offline"
				+"&approval_prompt=force";

function start_auth(req,res){
	res.redirect(g_auth_url);
};

function get_access_token(req,res,callback){
		if(req.query.code){
			var g_code_data={
				code:req.query.code,
				client_id:g_client_id,
				client_secret:g_client_secret,
				redirect_uri:g_redirect_uri,
				grant_type:"authorization_code"
			}
			request.post("https://accounts.google.com/o/oauth2/token",{form:g_code_data},function(err,headers,body){
				if(JSON.parse(body).access_token!=null){
					callback(JSON.parse(body));
				}
				//calback(JSON.parse(body).access_token);
			});	
		}
		else
		{
			return null;
			res.send("error");
		}
}
///



function list_files(req,res,folderId,maxResults,access_token,callback){
	request.get({url:"https://www.googleapis.com/drive/v2/files?q=%27"+folderId+"%27%20in%20parents",
		auth:{"bearer":access_token}},function(err,headers,body){
		callback(JSON.parse(body),res);
	});
}


	//if(folderId == "root"){folderId="";} https://www.googleapis.com/drive/v2/files/"+folderId+"/children

function list_folders(req,res,folderId,maxResults,access_token,callback){
	if(folderId=="root"){folderId=""};
		console.log(folderId);
		request.get({url:"https://www.googleapis.com/drive/v2/files/"+folderId+/*"?maxResults="+maxResults+*/
		"?fields=items(description%2CdownloadUrl%2CfileExtension%2CfileSize%2Cid%2CmodifiedDate%2Ctitle)",
		auth:{"bearer":access_token}},function(err,headers,body){
		callback(JSON.parse(body),res);
	});
}

function get_file_metadata(fileID,callback){
	if(folderId=="root"){folderId=""};
	request.get({url:"https://www.googleapis.com/drive/v2/files/"+fileId+
	"&fields=fields=description%2CdownloadUrl%2CfileExtension%2CfileSize%2Cid%2ClastModifyingUser%2ClastModifyingUserName%2Ctitle",
	auth:{"bearer":access_token}},function(err,headers,body){
			
		if (!error && response.statusCode == 200) {
			callback(JSON.parse(body));
		}else{
			return null;
		}
	});
}

module.exports={
	start_auth:start_auth,
	get_access_token:get_access_token,
	list_files:list_files,
	get_file_metadata:get_file_metadata,
	list_folders:list_folders
}