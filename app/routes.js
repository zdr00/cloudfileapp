var mongojs    = require('mongojs'); //Creates an object for mongodb
var User       = require('../app/models/user'); //Model of our user
var configAuth = require('../config/auth'); // Credentials for configuration
var db         = mongojs('cloudfiledb', ['users']); //Select the db and collection
var fs 		   = require('fs'); //creates an object to read files and uploads
var multer 	   = require('multer'); //Multer enables the upload of files
var copyapi    = require('copyapi');


module.exports = function(app, passport, User) {

app.use(multer({dest:'./uploads/'}));
app.all('/api/*', isLoggedIn); //Protects the api's routes
//app.all('/connect/*', isLoggedIn);
//app.all('/refreshtoken/*', isLoggedIn);


//// =================================================================================
//// COPY ROUTES =====================================================================
//// =================================================================================
	/*
	copyapi.configure({
	    consumer_key: '1tRZcyP37LbbNlruLP0QO0O75IDsz9ex',
	    consumer_secret: 'RWUvZM1B0Op0ukbMcvkQLEHGPR6dDaEpF2QIDgNjCHFdHcsc',
	    callback_url: 'http://www.cloudfileapp/connect/copy/get_access_token',
	});

	app.get('connect/copy/get_request_token', function(req, res) {
		console.log("GET /get_request_token");
		copyapi.getRequestToken(function(error, request_pair, redirect_url) {
		    if (error) { res.send("ERROR GETTING REQUEST TOKEN:\n" + error); return; }
		    // Store the Request Token and Request Secret in the users session
		    db.users.update({_id:req.user._id}, { $set: { 'copy.token':request_pair } },{upsert:true}, function() {
		    	req.session.request_pair = request_pair;
			    console.log("REQUEST TOKEN: " + request_pair.token + "\nREQUEST SECRET: " + request_pair.secret);
			    res.redirect(redirect_url);
		    });		    
		});
	});

*/
//// =================================================================================
//// BOX ROUTES ======================================================================
//// =================================================================================

	var box 		= require("./box");
	var boxID		= configAuth.boxAuth.clientID;
	var boxClient   = configAuth.boxAuth.clientSecret;
	var boxCallback = configAuth.boxAuth.callbackURL;

	app.get("/connect/box",function(req,res){
		box.config(boxID,boxClient,boxCallback);
		box.start_auth(res);
	});

	app.get("/connect/box/auth", isLoggedIn, function(req,res){
				box.get_access_token(res,req.query.code,function(token,error){
					db.users.update({_id:req.user._id}, { $set: { 'box.token':token } },{upsert:true}, function() {
					if(error){
						res.send("error");
					}
					else{
					  res.redirect("/");
					}
				});
			});
	});

	app.get("/api/files/box/root", isLoggedIn, function(req,res){
		if(req.user){
			db.users.findOne({_id:req.user._id}, function(err, doc) {
				if(err){
					res.render('profile.ejs',{error:'Ups! There was an error, try again later', user:req.user});
				}else{
					if(doc.box && doc.box.token){
						box.list_root_folder(res,doc.box.token.access_token,function(err,files){
							if(err){
								res.send("Error with box");
							}else{
								res.send(files);
							}
						});
					}else{
						res.redirect("/");
					}
				}
			});
		}else{
			res.redirect("/");
		}
	});

	app.get("/api/files/box/:id", isLoggedIn, function(req,res){
		if(req.user){
			db.users.findOne({_id:req.user._id}, function(err, doc) {
				if(err){
					res.render('profile.ejs',{error:'Ups! There was an error, try again later', user:req.user});
				}else{
					if(doc.box && doc.box.token){
						box.list_folder(res,req.params.id,doc.box.token.access_token,function(err,files){
							if(err){
								res.send("Error with box");
							}else{
								res.send(files);
							}
						});
					}else{
						res.redirect("/");
					}
				}
			});
		}else{
			res.redirect("/");
		}
	});

	app.get("/api/files/box/file/:id", isLoggedIn, function(req,res){
		if(req.user){
			db.users.findOne({_id:req.user._id}, function(err, doc) {
				if(err){
					res.render('profile.ejs',{error:'Ups! There was an error, try again later', user:req.user});
				}else{
					if(doc.box && doc.box.token){
						box.list_file(res,req.params.id,doc.box.token.access_token,function(err,files){
							if(err){
								res.send("Error with box");
							}else{
								res.send(files);
							}
						});
					}else{
						res.redirect("/");
					}
				}
			});
		}else{
			res.redirect("/");
		}
	});

	app.get("/refreshtoken/box", userData, function(req,res){
		var box_code_data={
			grant_type:"refresh_token",
			refresh_token:req.currentUser.box.token.refresh_token,
			client_id:configAuth.boxAuth.clientID,
			client_secret:configAuth.boxAuth.clientSecret
		}
		request.post("https://app.box.com/api/oauth2/token",{form:box_code_data},function(err,headers,body){
			db.users.update({_id:req.user._id}, { $set: { 'box.token':JSON.parse(body)} },{upsert:true}, function() {
				res.redirect("/");
			});
		});
	});

	app.get("/bfolder",isLoggedIn,function(req,res){
		res.render("viewfiles/bfiles.ejs",{
				id:req.query.id,
				name:req.query.name,
				type:req.query.type
			});
	});



//// =================================================================================
//// SKYDRIVE ROUTES =================================================================
//// =================================================================================

	/*
	app.get("/connect/skydrive",function(req,res){
		res.render("skydrive.ejs");
	});
	*/


//// =================================================================================
//// GOOGLE DRIVE ROUTES =============================================================
//// =================================================================================

	var gdrive = require("./gdrive");

	app.get("/connect/drive",function (req,res){
		gdrive.start_auth(req,res);
	});

	//api/files/google/root?token="+token
	app.get("/connect/gapi/auth", isLoggedIn, function (req,res){
		gdrive.get_access_token(req,res,function (token){
			if(req.user){
				db.users.update({_id:req.user._id}, { $set: { 'google.token':token.access_token,'google.refreshtoken':token.refresh_token} },{upsert:true}, function() {
					res.redirect("/");
				});
			}else{
				res.redirect('/');
			}
		});
	});

	app.get("/api/files/google/:folderid", isLoggedIn, function(req,res){
		//Dbox .. etc
		//Combinar.
		//Recuperar token de db"
		//"folder / = "root"
		//req.query.token
		if(req.user){
			db.users.findOne({_id:req.user._id}, function(err, doc) {
				if(err){
					res.render('profile.ejs',{error:'Ups! There was an error, try again later', user:req.user});
				}else{
					if(doc.google && doc.google.token){
							gdrive.list_files(req,res,req.params.folderid,10,doc.google.token,function(files,res){
									res.json(files);
							});
					} else {
						//res.redirect('/profile');
					}
				}
			});
		}else{
			res.redirect('/');
		}
	});

/*
	app.get("/api/files/google/folders/:folderid", function(req,res){
		//Dbox .. etc
		//Combinar.
		//Recuperar token de db"
		//"folder / = "root"
		//req.query.token
		db.users.findOne({_id:req.user._id}, function(err, doc) {
			if(err){
				res.render('profile.ejs',{error:'Ups! There was an error, try again later', user:req.user});
			}else{
				if(doc.google && doc.google.token){
						gdrive.list_folders(req,res,req.params.folderid,10,doc.google.token,function(files,res){
							res.json(files);
						});
				} else {
					//res.redirect('/profile');
				}
			}
		});

	});
*/
	app.get("/gfolder",isLoggedIn,function(req,res){
		res.render("viewfiles/gfiles.ejs",{
				ruta:req.query.ruta,
				name:req.query.name,
				url:req.query.url
			});
	});

	app.get('/refreshtoken/google', function (req,res){
		if(req.user){
			db.users.findOne({_id:req.user._id}, function(err, doc) {
				var g_code_data={
					client_id:configAuth.googleAuth.clientID,
					client_secret:configAuth.googleAuth.clientSecret,
					refresh_token:doc.google.refreshtoken,
					grant_type:"refresh_token"
				}
				request.post("https://accounts.google.com/o/oauth2/token",{form:g_code_data},function(err,headers,body){
					db.users.update({_id:req.user._id}, { $set: { 'google.token':JSON.parse(body).access_token} },{upsert:true}, function() {
						res.redirect("/");
					});
				});
			});
		}else{
			res.redirect("/");
		}
	});

//// =================================================================================
//// DROPBOX ROUTES ==================================================================
//// =================================================================================
	var dbox 	= require('dbox'),
		dropbox = dbox.app({"app_key":configAuth.dropboxAuth.DROPBOX_CLIENT_ID,"app_secret":configAuth.dropboxAuth.DROPBOX_CLIENT_SECRET,"root":"dropbox"}),
		link    = "",
		token 	= false;


	app.get('/connect/dropbox', function(req,res){
		dropbox.requesttoken(function(status, request_token){
			enlace = request_token.authorize_url+'&oauth_callback='+configAuth.dropboxAuth.DROPBOX_CALLBACKURL;
			token  = request_token;
			res.redirect(enlace);
		});
	});

	app.get('/connect/dropbox/auth', function(req,res){		
		if(req.user){
			dropbox.accesstoken(token, function(status, access_token){							
				db.users.update({_id:req.user._id}, { $set: { 'dropbox.token':access_token } },{upsert:true}, function() {
				    cliente = dropbox.client(access_token);
					res.redirect('/');
				});			
			});
		}else{
			res.redirect("/profile");
		}
	});

	app.get("/api/files/dropbox/root", function(req,res){
		if(req.user){
			db.users.findOne({_id:req.user._id}, function(err, doc) {
				if(err){
					res.render('profile.ejs',{error:'Ups! There was an error, try again later', user:req.user});
				}else{
					if(doc.dropbox && doc.dropbox.token){
							var dcliente = dropbox.client(doc.dropbox.token);
							dcliente.metadata(req.query.ruta, function(status, reply){
								res.json(reply);
						  });
					}
				}
			});
		}
	});

	app.get("/dfolder", isLoggedIn, function(req,res){
		if(req.query.ruta){
			res.render("viewfiles/dfiles.ejs",{ruta:req.query.ruta});
		}else{
			res.redirect('/');
		}
	});

	app.get("/api/file/previews/dropbox", isLoggedIn, userData, function(){
		//https://api-content.dropbox.com/1/previews/auto/<path>
	});

	app.get('/api/delete/dropbox', userData, function (req,res){
		var rmcliente = dropbox.client(req.currentUser.dropbox.token);
		    rmcliente.rm(req.query.file, function(status, reply){
				res.redirect("/");
			});
	});

	app.get('/api/create/dropbox', userData, function(req,res){
		var createDbox = dropbox.client(req.currentUser.dropbox.token);
		    createDbox.mkdir(req.query.path, function(status, reply){
				res.redirect("/");
			});
	});

	app.get('/api/download/dropbox', userData, function(req,res){
		var client = dropbox.client(req.currentUser.dropbox.token);
			client.get(req.query.file,  function(status, reply, metadata){
				var file = req.query.file.substring(req.query.file.lastIndexOf("/"));
				fs.writeFile(file, reply, function (err) {
				  if (err) throw err;
				  res.download(file);
				});
			});
	});

	app.get('/api/view/files/dropbox', userData, function(req,res){
		var client = dropbox.client(req.currentUser.dropbox.token);
		    client.shares(req.query.file, function(status, reply){
				res.json(reply);
			});
	});

	app.get('/api/files/dropbox/all', userData, function(req,res){
		var dboxcliente = dropbox.client(req.currentUser.dropbox.token);
			dboxcliente.delta(function(status, reply){
				res.json(reply.entries);
			});
	});

	app.get('/api/account/dropbox', userData, function(req,res){
		var objeto = dropbox.client(req.currentUser.dropbox.token);
		    objeto.account(function(status, reply){
				res.json(reply);
			});
	});

	app.post("/api/upload/dropbox", userData, function(req,res){
		/*var client = dropbox.client(req.currentUser.dropbox.token);
			console.log(req.body.file);

			client.put(req.body.file, req.body.file, function(status, reply){
				res.redirect("/");
			})
		*/
		fs.readFile(req.files.file.path, function (err, data) {
		  // ...
		var client = dropbox.client(req.currentUser.dropbox.token);
			client.put(req.files.file.originalname, data, function(status, reply){
				res.redirect("/");
			});
		});
	});


// NORMAL ROUTES ====================================================================

	app.get('/', function(req, res) {
		if(req.user){
				db.users.findOne({_id:req.user._id}, function(err, doc) {
					if(err){
						res.render('profile.ejs',{error:'Ups! There was an error, try again later', user:req.user});
					}else{

						//if(doc.local.plan=='disabled'){
						//		res.render('profile.ejs',{error:'Please complete your payment on billing to start', user:req.user});
						//}else{
							if(doc.dropbox && doc.dropbox.token || doc.google && doc.google.token || doc.skydrive && doc.skydrive.token || doc.box && doc.box.token){
										res.render('home.ejs',{usuario:req.user});
							 }else{
							 	//if(doc.local.plan=='disabled'){
							 	//	res.render('profile.ejs',{error:'Please complete your payment on billing', user:req.user});
							 	//}else{
							 		res.render('profile.ejs',{error:'Please add an account to start', user:req.user});
							 	//}
							 }
						//}
					}
				});
		}else{
			res.render('index.ejs');
		}
	});

	app.get('/success', isLoggedIn, function(req, res){
		db.users.update({_id:req.user._id}, { $set: { 'local.plan':'enabled'} },{upsert:true}, function() {
			 res.redirect("/");
		});
	});

// UPGRADE SECTION ====================================================================

	app.get('/billing', isLoggedIn, function(req,res){
		res.render('billing.ejs',{
			user: req.user
		});
	});
// PROFILE SECTION =========================
	app.get('/profile', isLoggedIn, function(req, res) {
		res.render('profile.ejs', {
			user : req.user
		});
	});

	app.get('/sitemap', function(req, res) {
		res.render('sitemap.ejs');
	});
////SHARE LINKS////////////////////////////////////////////

	app.get('/share/dropbox/', function(req,res){
		if(req.user){
			db.users.findOne({_id:req.user._id}, function(err, doc) {
				var client = dropbox.client(doc.dropbox.token);
			    client.shares("/"+req.query.link, {'root':'dropbox'}, function(status, reply){
					res.render('share.ejs',{
						name:req.query.link,
						link:reply.url
					});
				});
			});
		}else{
			res.redirect('/');
		}
	});


	app.get('/share/drive/', function(req,res){
		if(req.user){
				res.render('share.ejs',{
					name:req.query.drive,
					drivelink:req.query.drivelink
				});
		}else{
			res.redirect('/');
		}
	});

	// LOGOUT ==============================
	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

// =============================================================================
// DOWNLOAD (DROPBOX) ==================================================
// =============================================================================

/*
app.use(function(err, req, res, next){
	if(404==err.status){
		res.statusCode=404;
		res.send('Cant find the module');
	} else {
		next(err);
	}
});
*/


// =============================================================================
// AUTHENTICATE (FIRST LOGIN) ==================================================
// =============================================================================

// locally --------------------------------
	// LOGIN ===============================
	// show the login form
	app.get('/login', function(req, res) {
		if(req.user){
			/*
			res.render('login.ejs',{
				user : req.user,
				action: '/login',
				title : 'Sign in'
			});
			*/
			res.redirect('/');
		}else{
			res.render('login.ejs', { message: req.flash('loginMessage'), title: "CloudFile" });
		}
	});

	// process the login form
	app.post('/login', passport.authenticate('local-login', {
		successRedirect : '/', // redirect to the secure profile section
		failureRedirect : '/login', // redirect back to the signup page if there is an error
		failureFlash : true,
		title: 'CloudFile'// allow flash messages
	}));

	// SIGNUP =================================
	// show the signup form
	app.get('/signup', function(req, res) {
		if(req.user){
			res.render('signup.ejs',{
				user : req.user,
				action : '/signup',
				title : 'Create an account'
			});
		}else{
			res.render('signup.ejs', { message: req.flash('signupMessage'), title: "CloudFile"});
		}
	});

	// process the signup form
	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/', // redirect to the secure profile section
		failureRedirect : '/signup', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

// facebook -------------------------------

	// send to facebook to do the authentication
	app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));

	// handle the callback after facebook has authenticated the user
	app.get('/auth/facebook/callback',
		passport.authenticate('facebook', {
			successRedirect : '/',
			failureRedirect : '/'
		}));


// =============================================================================
// AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
// =============================================================================

	// facebook -------------------------------

	// send to facebook to do the authentication
	app.get('/connect/facebook', passport.authorize('facebook', { scope : 'email' }));

	// handle the callback after facebook has authorized the user
	app.get('/connect/facebook/callback',
		passport.authorize('facebook', {
			successRedirect : '/',
			failureRedirect : '/'
		}));




// =============================================================================
// UNLINK ACCOUNTS =============================================================
// =============================================================================
// used to unlink accounts. for social accounts, just remove the token
// for local account, remove email and password
// user account will stay active in case they want to reconnect in the future

	// local -----------------------------------
	app.get('/unlink/local', function(req, res) {
		var user            = req.user;
		user.local.email    = undefined;
		user.local.password = undefined;
		user.save(function(err) {
			res.redirect('/profile');
		});
	});

	// facebook -------------------------------
	app.get('/unlink/facebook', function(req, res) {
		var user            = req.user;
		user.facebook.token = undefined;
		user.save(function(err) {
			res.redirect('/profile');
		});
	});

	app.get('/unlink/dropbox', function(req, res) {
		var user           = req.user;
		user.dropbox.token = undefined;
		user.save(function(err) {
			res.redirect('/profile');
		});
	});

	app.get('/unlink/drive', function(req, res) {
		var user          		 = req.user;
		user.google.token 		 = undefined;
		user.google.refreshtoken = undefined;
		user.save(function(err) {
			res.redirect('/profile');
		});
	});

	app.get('/unlink/box', function(req, res) {
		var user       = req.user;
		user.box.token = undefined;
		user.save(function(err) {
			res.redirect('/profile');
		});
	});

	app.get('*', function(req,res){
		res.render('partials/404.ejs',{
			url:req.url
		});
	});
};

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
	if (req.isAuthenticated())
		return next();

	res.redirect('/');
}

function userData(req, res, next){
	if(req.user){
		db.users.findOne({_id:req.user._id},function(err,doc){
			req.currentUser = doc;
			return next();
		});
	}
}
