var User       = require('../app/models/user');
var mongojs    = require('mongojs');
var db         = mongojs('cloudfiledb', ['users']);

var express 	= require('express'),
	app 		= express(),
	configAuth 	= require('../config/auth');

var dbox 	= require('dbox'),
	dropbox = dbox.app({"app_key":configAuth.dropboxAuth.DROPBOX_CLIENT_ID,"app_secret":configAuth.dropboxAuth.DROPBOX_CLIENT_SECRET}),
	link    = "",
	token 	= false;

function start_auth(res){
	dropbox.requesttoken(function(status, request_token){
		enlace = request_token.authorize_url+'&oauth_callback=http://www.cloudfileapp.com/connect/dropbox/auth';
		token  = request_token;
		res.redirect(enlace);
	});
}

function save_token(req,res){
	dropbox.accesstoken(token, function(status, access_token){
		db.users.update({_id:req.user._id}, { $set: { 'dropbox.token':access_token } },{upsert:true}, function() {
		    cliente = dropbox.client(access_token);
   	  		res.redirect('/');
		});
	});
}

module.exports={
	start_auth:start_auth,
	save_token:save_token
}