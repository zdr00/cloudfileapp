var box_client_id,box_client_secret,box_redirect_uri;
var request			=	require("request");
var box_oauth_url	=	"https://app.box.com/api/oauth2/authorize";
var box_oauth_token	=	"https://app.box.com/api/oauth2/token";
function config(cid,sid,r_uri){
	box_client_id=cid;
	box_client_secret=sid;
	box_redirect_uri=r_uri;
}
function start_auth(res){
	var url=box_oauth_url+"?response_type=code"+
	"&client_id="+box_client_id+
	"&redirect_uri"+box_redirect_uri+
	"&state=default";
	res.redirect(url);
}
function get_access_token(res,code,cb){
    var post_data={
    	grant_type:"authorization_code",
    	code:code,
    	client_id:box_client_id,
    	client_secret:box_client_secret,
    	redirect_uri:box_redirect_uri
    }
    request.post(box_oauth_token,{form:post_data},function(err,headers,body){
		cb(JSON.parse(body),err);
    });
}
function list_root_folder(res,token,cb){
	request.get('https://api.box.com/2.0/folders/0',
	{
		auth: {'bearer':token}
	},
	function(err,headers,body){
		if(err){
			console.log(err);
		}else{
			cb(err,body);
			}
	});
}

function list_folder (res,folder,token,cb) {
	request.get('https://api.box.com/2.0/folders/'+folder+'/items',
	{auth: {'bearer':token}},function(err,headers,body){
		if(err){
			console.log(err);
		}else{
			cb(err,body);
		}
	});
}

function list_file(res,file,token,cb){
	request.get('https://api.box.com/2.0/files/'+file+'/items',
	{auth: {'bearer':token}},function(err,headers,body){
		if(err){
			console.log(err);
		}else{
			cb(err,body);
		}
	});
}

module.exports={
	list_root_folder:list_root_folder,
	list_folder:list_folder,
	list_file:list_file,
	get_access_token:get_access_token,
	start_auth:start_auth,
	config:config
}
